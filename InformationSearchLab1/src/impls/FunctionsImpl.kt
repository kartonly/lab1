package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b
    override fun substringCounter(s: String, sub: String): Int {
        var x:Int = 0
        for(char in s.toCharArray()){
            if (char.toString() == sub) {
                x++
            }
        }
        return x
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        return s.split(sub).toList()
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        var x = " "
        val res = mutableMapOf<String, Int>()
        for(char in s.split(sub)){
            if (res.contains(char)) {
                res.put(char, res.getValue(char).inc())
            } else {
                res += Pair(char, 1)
            }
        }
        return res
    }

    override fun isPalindrome(s: String): Boolean {
        if (s == ""){
            return false
        } else {
            var revers = s.reversed()
            if (revers == s) {
                return true
            } else {
                return false
            }
        }
    }

    override fun invert(s: String): String {
        return s.reversed()
    }
}
